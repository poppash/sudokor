from typing import Any, Callable

from PyQt5.QtWidgets import QApplication, QMessageBox, QPushButton

from sudokor import NOOP


class SolvePuzzleButton(QPushButton):
    def configure(self, callback: Callable[[], Any] = NOOP):
        self.setText("Solve")

        self.callback = callback
        self.clicked.connect(self.prompt)

    def prompt(self) -> None:
        confirmation = QMessageBox.question(
            None,  # type: ignore[arg-type]
            "Solve",
            "Are you sure you want to solve the puzzle?",
            QMessageBox.Yes | QMessageBox.No,
        )

        if confirmation == QMessageBox.Yes:
            self.execute()

    def execute(self) -> Any:
        return self.callback()


class ResetPuzzleButton(QPushButton):
    def configure(self, callback: Callable[[], Any] = NOOP):
        self.setText("Reset")

        self.callback = callback
        self.clicked.connect(self.prompt)

    def prompt(self) -> None:
        confirmation = QMessageBox.question(
            None,  # type: ignore[arg-type]
            "Reset",
            "Are you sure you want to reset?",
            QMessageBox.Yes | QMessageBox.No,
        )

        if confirmation == QMessageBox.Yes:
            self.execute()

    def execute(self) -> Any:
        return self.callback()


class QuitButton(QPushButton):
    def __init__(self):
        super().__init__()
        self.configure()

    def configure(self):
        self.setText("Quit")
        self.clicked.connect(self.prompt)

    def prompt(self) -> None:
        confirmation = QMessageBox.question(
            None,  # type: ignore[arg-type]
            "Quit",
            "Are you sure you want to quit Sudokor UI?",
            QMessageBox.Yes | QMessageBox.No,
        )

        if confirmation == QMessageBox.Yes:
            self.execute()

    def execute(self) -> None:
        return QApplication.quit()
