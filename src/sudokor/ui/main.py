import sys
from typing import Any, Callable

from PyQt5.QtWidgets import (
    QApplication,
    QComboBox,
    QDesktopWidget,
    QMainWindow,
    QVBoxLayout,
    QWidget,
)

from sudokor.ui.buttons import QuitButton
from sudokor.ui.puzzles import (
    BasePuzzle,
    CalcudokuPuzzle,
    SudokuPuzzle,
    TwinSudokuPuzzle,
)

APP_NAME = "Sudokor UI"
APP_GEOMETRY = (0, 0, 400, 300)

PUZZLE_SELECTOR_PLACEHOLDER_TEXT = "Please select a puzzle..."
PUZZLE_SELECTOR_PUZZLE_TYPES = ["Sudoku", "Twin Sudoku", "Calcudoku"]


class SudokorUiPuzzleSelector(QComboBox):
    def configure(self, items: list[str], callback: Callable[[str], Any]):
        items = items or []
        self.addItems(items)
        self.currentTextChanged.connect(callback)


class SudokorUiControls(QWidget):
    def __init__(self):
        super().__init__()
        self.configure()

    def configure(self):
        self.layout = layout = QVBoxLayout()
        self.setLayout(layout)

        quit_button = QuitButton()
        layout.addWidget(quit_button)


class SudokorUiPuzzleContainer(QWidget):
    def configure(self, puzzle: BasePuzzle) -> None:
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.layout().addWidget(puzzle)


class SudokorUi(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setWindowTitle(APP_NAME)
        self.setGeometry(*APP_GEOMETRY)
        self.center()

        widget = QWidget()
        self.setCentralWidget(widget)

        layout = QVBoxLayout()
        widget.setLayout(layout)

        self.puzzle_selector = puzzle_selector = SudokorUiPuzzleSelector()
        puzzle_selector.configure(
            items=[PUZZLE_SELECTOR_PLACEHOLDER_TEXT] + PUZZLE_SELECTOR_PUZZLE_TYPES,
            callback=self.select_puzzle,
        )

        layout.addWidget(puzzle_selector)

        self.puzzle_container = puzzle_container = SudokorUiPuzzleContainer()
        layout.addWidget(puzzle_container)

        layout.addStretch()

        self.controls = controls = SudokorUiControls()
        layout.addWidget(controls)

    def center(self):
        screen = QDesktopWidget().screenGeometry()
        window = self.geometry()
        x = (screen.width() - window.width()) // 2
        y = (screen.height() - window.height()) // 2
        self.move(x, y)

    def select_puzzle(self, puzzle: str) -> None:
        if puzzle not in PUZZLE_SELECTOR_PUZZLE_TYPES:
            raise ValueError(f"{puzzle}: str not in {PUZZLE_SELECTOR_PUZZLE_TYPES}")

        self.puzzle_selector.hide()

        if puzzle == "Sudoku":
            self.puzzle_container.configure(puzzle=SudokuPuzzle())
        elif puzzle == "Twin Sudoku":
            self.puzzle_container.configure(puzzle=TwinSudokuPuzzle())
        elif puzzle == "Calcudoku":
            self.puzzle_container.configure(puzzle=CalcudokuPuzzle())


def app() -> None:
    app = QApplication(sys.argv)

    ui = SudokorUi()
    ui.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    app()
