from PyQt5.QtWidgets import QMessageBox


class NoSolutionFoundAlert(QMessageBox):
    WINDOW_TITLE = "No solution"
    TEXT = "No solution was found - make sure to provide a valid puzzle!"

    def __init__(self, show: bool = True):
        super().__init__(
            QMessageBox.Warning,
            self.WINDOW_TITLE,
            self.TEXT,
            buttons=QMessageBox.Ok,
        )
        if show:
            self.exec()
