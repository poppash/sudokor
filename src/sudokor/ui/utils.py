from typing import Any, Callable, Iterable

import colorutils  # type: ignore


def iter_list_of_lists(x: list[list[Any]]) -> Iterable[Any]:
    for row in x:
        yield from row


def random_hex_color() -> str:
    return colorutils.random_web()


def try_parse(x: Any, fn: Callable[[Any], Any]) -> Any | None:
    try:
        return fn(x)
    except (TypeError, ValueError):
        return None


def try_int(x: Any) -> int | None:
    return try_parse(x, int)


def try_float(x: Any) -> float | None:
    return try_parse(x, float)
