from __future__ import annotations


class StyleSheet:
    def __init__(self, data: dict[str, str] | None = None):
        self.data = data or {}

    def __str__(self):
        return self.to_str()

    def __repr__(self):
        return self.to_str()

    def has_style(self, key: str) -> bool:
        return key in self.data.keys()

    @classmethod
    def from_merger(cls, a: StyleSheet, b: StyleSheet) -> StyleSheet:
        return cls({**a.data, **b.data})

    @classmethod
    def from_str(cls, x: str) -> StyleSheet:
        rv = {}
        for el in x.split(";"):
            if not el:
                break
            k, v, *_ = [x.strip() for x in el.split(":")]
            rv[k] = v
        return cls(rv)

    def to_str(self):
        return " ".join([f"{k}: {v};" for k, v in self.data.items()])
