import logging
from typing import Iterable

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout, QLineEdit, QWidget

from sudokor.constructors import from_matrix, to_matrix
from sudokor.types import Matrix
from sudokor.ui.alerts import NoSolutionFoundAlert
from sudokor.ui.puzzles import BaseCell, BasePuzzle
from sudokor.ui.utils import iter_list_of_lists, try_int
from sudokor.variations.sudoku import solve

logger = logging.getLogger(__name__)


class Cell(QLineEdit, BaseCell):
    @staticmethod
    def select_color(i: int, j: int) -> str:
        box_size = 3
        b_row = i // box_size
        b_col = j // box_size
        b = 3 * b_row + b_col

        colors = ["#4d4d4d", "#7f8c8d"]
        return colors[b % 2]

    def __init__(self, i: int, j: int) -> None:
        super().__init__()

        self.i = i
        self.j = j

        self.setAlignment(Qt.AlignCenter)  # type: ignore[attr-defined]
        self.textChanged.connect(self.onTextChanged)

        self.set_background(self.select_color(i, j))

    def onTextChanged(self) -> None:
        width = self.fontMetrics().width(self.text())
        self.setMinimumWidth(width)

    def set_background(self, color: str, *args, **kwargs) -> None:
        self.setStyleSheet(f"background-color: {color};")

    def clear(self) -> None:
        super().clear()


class SudokuPuzzle(BasePuzzle):
    def __init__(self):
        super().__init__()

        self.grid = QWidget()
        self.grid.setLayout(QGridLayout())
        self.layout().addWidget(self.grid)

        for i in range(9):
            row = []
            for j in range(9):
                cell = Cell(i, j)
                row.append(cell)
                self.grid.layout().addWidget(cell, i, j)
            self.cells.append(row)

    def iter_cells(self) -> Iterable[Cell]:
        return iter_list_of_lists(self.cells)

    def solve(self) -> None:
        matrix: Matrix = [[try_int(cell.text()) for cell in row] for row in self.cells]

        x, xs = from_matrix(matrix), None
        try:
            logger.debug(f"About to solve: {to_matrix(x)}.")
            xs = solve(x)
        except Exception:
            NoSolutionFoundAlert()

        if xs is None:
            return

        xs_as_matrix = to_matrix(xs)
        logging.debug(f"Found solution: {xs_as_matrix}.")

        for i, row in enumerate(xs_as_matrix):
            for j, value in enumerate(row):
                text = str(value)
                self.cells[i][j].setText(text)

    def reset(self):
        for cell in self.iter_cells():
            cell.clear()


if __name__ == "__main__":
    import sys

    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)

    ui = SudokuPuzzle()

    ui.setWindowTitle("Calcudoku")
    ui.show()

    sys.exit(app.exec_())
