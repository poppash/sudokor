import logging
from typing import Callable, TypeAlias

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (
    QComboBox,
    QGridLayout,
    QHBoxLayout,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from sudokor.constructors import from_shape, to_matrix
from sudokor.ui.alerts import NoSolutionFoundAlert
from sudokor.ui.puzzles import BaseCell, BasePuzzle
from sudokor.ui.stylesheet import StyleSheet
from sudokor.ui.utils import iter_list_of_lists, random_hex_color, try_int
from sudokor.variations.calcudoku import solve

logger = logging.getLogger(__name__)

Coordinate: TypeAlias = tuple[int, int]
Constraint: TypeAlias = tuple[list[Coordinate], str, int]
Callback: TypeAlias = Callable | None

COLORS = {
    # see https://develop.kde.org/hig/style/color/dark/
    "default": QColor("#31363b"),
    "active": QColor("#bdc3c7"),
}

SYMBOLS = ["+", "−", "×", "÷"]
SYMBOLS_MAP = {"+": "+", "−": "-", "×": "*", "÷": "/"}


def NOOP(*args, **kwargs) -> None: ...


class Cell(QPushButton, BaseCell):
    DEFAULT_STYLESHEET = {"color": "white"}

    def __init__(
        self,
        i: int,
        j: int,
        on_active: Callback = NOOP,
        on_inactive: Callback = NOOP,
        enabled: bool = True,
    ) -> None:
        super().__init__()
        self.i: int = i
        self.j: int = j

        self.active: bool = False
        self.on_active = on_active
        self.on_inactive = on_inactive

        self.clicked.connect(self.on_clicked)

        if not enabled:
            self.disable()
        self.fix_size()

        stylesheet = StyleSheet(self.DEFAULT_STYLESHEET)
        self.apply(stylesheet, reset=True)

        self.has_background = False
        self.has_border = False
        self.has_text = False

    def toggle_active(self):
        self.active = not self.active
        self.refresh()

    def refresh(self):
        if self.active:
            color = COLORS["active"]
        else:
            color = COLORS["default"]

        self.set_background(color)

    def on_clicked(self):
        self.toggle_active()

        coordinate = self.i, self.j

        if self.active:
            self.on_active(coordinate)
        else:
            self.on_inactive(coordinate)

    def fix_size(self):
        hint = self.sizeHint()
        self.setFixedSize(hint)

    def set_background(self, color: QColor) -> None:
        s = StyleSheet({"background-color": f"{color.name()}"})
        self.apply(s)
        self.has_background = True

    def set_border(self, color: QColor) -> None:
        s = StyleSheet(
            {
                "border-style": "solid",
                "border-width": "1px",
                "border-color": color.name(),
            }
        )
        self.apply(s)
        self.has_border = True

    def set_styling(
        self, background_color: QColor | None = None, border_color: QColor | None = None
    ) -> None:
        if background_color:
            self.set_background(background_color)
        if border_color:
            self.set_border(border_color)

    def reset_styling(self):
        self.apply(StyleSheet(self.DEFAULT_STYLESHEET), reset=True)
        self.has_background = False
        self.has_border = False

    def set_text(self, text: str) -> None:
        self.setText(text)
        self.has_text = True

    def reset_text(self) -> None:
        self.setText("")
        self.has_text = False

    def enable(self) -> None:
        self.setEnabled(True)

    def disable(self) -> None:
        self.setEnabled(False)

    def clear(self):
        self.reset_text()
        self.reset_styling()


class CalcudokuConstraintComposer(QWidget):
    def __init__(
        self,
        on_add: Callback = NOOP,
        unavailable_coordinates: list[Coordinate] | None = None,
    ):
        super().__init__()

        self.coordinates: list[Coordinate] = []
        self.unavailable_coordinates = unavailable_coordinates or []

        self.operation: str | None = None
        self.target: str | None = None

        self.on_add = on_add

        self.setLayout(QVBoxLayout())

        self.grid = QWidget()
        self.grid.setLayout(QGridLayout())

        self.layout().addWidget(self.grid)

        for i in range(6):
            row = []
            for j in range(6):
                cell = Cell(i, j, self.add_coordinate, self.remove_coordinate)
                if (i, j) in self.unavailable_coordinates:
                    cell.disable()
                self.grid.layout().addWidget(cell, i, j)  # type: ignore[call-arg]
            row.append(cell)

        hori = QWidget()
        hori.setLayout(QHBoxLayout())
        self.layout().addWidget(hori)

        operation_combobox = QComboBox()
        operation_combobox.addItems(["", *SYMBOLS])
        operation_combobox.currentTextChanged.connect(self.set_operation)
        self.layout().addWidget(operation_combobox)

        target_line_edit = QLineEdit()
        target_line_edit.textChanged.connect(self.set_target)
        self.layout().addWidget(target_line_edit)

        add_button = QPushButton("Add")
        add_button.clicked.connect(self.add_constraint)
        self.layout().addWidget(add_button)

        quit_button = QPushButton("Quit")
        quit_button.clicked.connect(lambda: self.close())  # type: ignore[arg-type]
        self.layout().addWidget(quit_button)

    def set_operation(self, operation: str) -> None:
        self.operation = operation

    def set_target(self, target: str) -> None:
        self.target = target

    def add_coordinate(self, coordinate: Coordinate) -> None:
        self.coordinates.append(coordinate)

    def remove_coordinate(self, coordinate: Coordinate) -> None:
        self.coordinates.remove(coordinate)

    ###

    def add_constraint(self):
        constraint = (self.coordinates, self.operation, try_int(self.target))
        self.on_add(constraint)
        self.close()


class CalcudokuPuzzle(BasePuzzle):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.grid = QWidget()
        self.grid.setLayout(QGridLayout())

        self.layout().addWidget(self.grid)

        for i in range(6):
            row = []
            for j in range(6):
                cell = Cell(i, j, enabled=False)
                row.append(cell)
                self.grid.layout().addWidget(cell, i, j)
            row.append(cell)

            self.cells.append(row)

        self.constraint_composer = None
        self.constraints = []

        new_button = QPushButton("New")
        new_button.clicked.connect(self.show_constraint_composer)
        self.layout().addWidget(new_button)

        undo_button = QPushButton("Undo")
        undo_button.clicked.connect(self.undo_add_constraint)
        self.layout().addWidget(undo_button)

        self.refresh()

    def iter_cells(self):
        return iter_list_of_lists(self.cells)

    def coordinates_in_use(self) -> list[Coordinate]:
        from itertools import chain

        return list(chain(*[coordinates for (coordinates, _, _) in self.constraints]))

    def add_constraint(self, constraint: Constraint) -> None:
        logger.debug(f"Adding constraint: {constraint}.")
        self.constraints.append(constraint)
        logger.debug(f"Current constraints: {self.constraints}.")
        self.refresh()

    def undo_add_constraint(self):
        if len(self.constraints) > 0:
            constraint = self.constraints.pop()
            logger.debug(f"Undo adding constraint: {constraint}.")
        else:
            logger.debug("Nothing to undo.")
        logger.debug(f"Current constraints: {self.constraints}.")
        self.refresh()

    def show_constraint_composer(self) -> None:
        self.constraint_composer = constraint_composer = CalcudokuConstraintComposer(
            on_add=self.add_constraint,
            unavailable_coordinates=self.coordinates_in_use(),
        )

        constraint_composer.setWindowTitle("Constraint Composer")
        constraint_composer.show()

    def refresh(self):
        for coordinates, operation, target in self.constraints:
            color = QColor(random_hex_color())
            for i, j in coordinates:
                cell = self.cells[i][j]
                if not cell.has_border:
                    self.cells[i][j].set_border(color)

                text = f"{target}{operation}"
                self.cells[i][j].set_text(text)

        coordinates_in_use = self.coordinates_in_use()
        m = n = len(self.cells)
        for i in range(m):
            for j in range(n):
                if (i, j) not in coordinates_in_use:
                    self.cells[i][j].clear()

    def solve(self):
        x = from_shape(6)
        c = [(c, SYMBOLS_MAP[s], t) for (c, s, t) in self.constraints]

        xs = None
        try:
            logger.info(f"About to solve: {to_matrix(x)} with constraints: {c}.")
            xs = solve(x, constraints=c)
        except Exception:
            NoSolutionFoundAlert()

        if xs is None:
            return

        xs_as_matrix = to_matrix(xs)
        logging.info(f"Found solution: {xs_as_matrix}.")

        for i, row in enumerate(xs_as_matrix):
            for j, value in enumerate(row):
                text = str(value)
                self.cells[i][j].setText(text)

    def reset(self):
        self.constraints = []
        for cell in self.iter_cells():
            cell.clear()


if __name__ == "__main__":
    import sys

    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)

    ui = CalcudokuPuzzle()

    ui.setWindowTitle("Calcudoku")
    ui.show()

    sys.exit(app.exec_())
