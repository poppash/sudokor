from PyQt5.QtWidgets import QVBoxLayout, QWidget

from sudokor.ui.buttons import ResetPuzzleButton, SolvePuzzleButton
from sudokor.ui.stylesheet import StyleSheet


class QWidgetWithStyleSheetSupport(QWidget):
    def apply(self, other: dict[str, str] | StyleSheet, reset: bool = False) -> None:
        current = StyleSheet.from_str(self.styleSheet())
        other = other if isinstance(other, StyleSheet) else StyleSheet(other)

        stylesheet = other if reset else StyleSheet.from_merger(current, other)
        self.setStyleSheet(stylesheet.to_str())


class BaseCell(QWidgetWithStyleSheetSupport): ...


class BasePuzzle(QWidget):
    def __init__(self) -> None:
        super().__init__()

        layout = QVBoxLayout()
        self.setLayout(layout)

        solve_puzzle_button = SolvePuzzleButton()
        solve_puzzle_button.configure(self.solve)

        self.layout().addWidget(solve_puzzle_button)

        reset_puzzle_button = ResetPuzzleButton()
        reset_puzzle_button.configure(self.reset)

        self.layout().addWidget(reset_puzzle_button)

        self.cells: list[list[BaseCell]] = []

    def solve(self):
        raise NotImplementedError

    def reset(self):
        raise NotImplementedError
