from typing import Callable, TypeAlias, TypeVar

T = TypeVar("T")

Matrix: TypeAlias = list[list[T]]
Sudoku: TypeAlias = Matrix[int | None]

Coordinate: TypeAlias = tuple[int, int]
Option: TypeAlias = tuple[Coordinate, list[int]]

Solver: TypeAlias = Callable[[Sudoku], Sudoku | None]
