from typing import Any

from sudokor.types import Matrix, Sudoku
from sudokor.utils import shape


def from_shape(m: int, n: int | None = None, default: Any = None) -> Matrix[Any]:
    n = n or m
    matrix = [[default for _ in range(n)] for _ in range(m)]

    return from_matrix(matrix)


def from_matrix(x: Matrix) -> Sudoku:
    return x


def to_matrix(s: Sudoku) -> Matrix:
    return s


def from_csv(x: str) -> Sudoku:
    def parse_int(value: str) -> int | None:
        try:
            return int(value)
        except ValueError:
            return None

    data = [
        [parse_int(value) for value in row.split(",")]
        for row in x.strip().split("\n")
        if row
    ]

    return from_matrix(data)


def from_sudoku(x: Sudoku, empty: bool = False) -> Sudoku:
    from copy import deepcopy

    if not empty:
        return deepcopy(x)

    m, n = shape(x)
    return from_shape(m, n)
