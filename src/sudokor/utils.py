from functools import wraps
from typing import Any, Callable, Generator

from sudokor.types import Coordinate, Matrix, Sudoku


def are_equal(x1: Sudoku | None, x2: Sudoku | None) -> bool:
    if x1 is None or x2 is None:
        return False

    return x1 == x2


def enumerate_matrix(x: Matrix) -> Generator[tuple[Coordinate, int | None], None, None]:
    m, n = shape(x)

    for i, row in enumerate(x):
        for j, value in enumerate(row):
            yield (i, j), value


def flatten(x: Matrix) -> list[int | None]:
    return [el for row in x for el in row]


def get_row(x: Sudoku, k: int) -> list[int | None]:
    return x[k]


def get_column(x: Sudoku, k: int) -> list[int | None]:
    return [row[k] for row in x]


def get_row_at_ij(s: Sudoku, i: int, j: int) -> list[int | None]:
    return get_row(s, i)


def get_column_at_ij(s: Sudoku, i: int, j: int) -> list[int | None]:
    return get_column(s, j)


def get_box_at_ij(s: Sudoku, i: int, j: int, size: int = 3) -> list[int | None]:
    i0, j0 = int(i / size) * size, int(j / size) * size
    return flatten([row[j0 : j0 + size] for row in s[i0 : i0 + size]])


def has_duplicates(values: list[int | None], ignore_none: bool = True) -> bool:
    net_values: list[int | None]

    if ignore_none:
        net_values = [el for el in values if el]
    else:
        net_values = values

    return len(net_values) != len(set(net_values))


def has_empty_cells(s: Sudoku) -> bool:
    return any(el is None for el in flatten(s))


def has_no_empty_cells(s: Sudoku) -> bool:
    return not has_empty_cells(s)


def shape(s: Sudoku) -> tuple[int, int]:
    return len(s), len(s[0])


def try_or_none(func) -> Callable[..., Any]:
    @wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        try:
            result = func(*args, **kwargs)
            return result
        except Exception:
            return None

    return wrapper


def without_none(x: list[int | None]) -> list[int]:
    return [el for el in x if el is not None]
