from functools import partial

import sudokor.solver as solver
from sudokor.solver import add_option
from sudokor.types import Option, Solver, Sudoku
from sudokor.utils import (
    flatten,
    get_box_at_ij,
    get_column,
    get_column_at_ij,
    get_row,
    get_row_at_ij,
    has_duplicates,
    shape,
)

NUMBERS: set = {1, 2, 3, 4, 5, 6, 7, 8, 9}


def is_valid(s: Sudoku) -> bool:
    m, n = shape(s)
    if m != 9 or n != 9:
        return False

    if not all([el in NUMBERS or el is None for el in flatten(s)]):
        return False

    for k in range(9):
        row, column = get_row(s, k), get_column(s, k)
        if has_duplicates(row) or has_duplicates(column):
            return False

    for i in range(3):
        for j in range(3):
            box = get_box_at_ij(s, i * 3, j * 3)
            if has_duplicates(box):
                return False

    return True


def find_options(s: Sudoku, exhaustive: bool = False) -> list[Option]:
    rv: list[Option] = []

    for i in range(9):
        for j in range(9):
            if s[i][j] is not None:
                continue

            non_options_ij = (
                get_row_at_ij(s, i, j)
                + get_column_at_ij(s, i, j)
                + get_box_at_ij(s, i, j)
            )

            options_ij = [el for el in NUMBERS if el not in non_options_ij]
            rv = add_option(rv, (i, j), options_ij)

            if not exhaustive and len(options_ij) <= 1:
                return rv
    return rv


def build_solver() -> Solver:
    return partial(solver.solve, find_options=find_options, is_valid=is_valid)


def solve(x: Sudoku) -> Sudoku | None:
    f = build_solver()
    return f(x)
