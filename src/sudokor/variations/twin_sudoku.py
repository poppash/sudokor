from functools import partial

import sudokor.solver as solver
from sudokor.solver import add_option
from sudokor.types import Option, Solver, Sudoku
from sudokor.utils import (
    flatten,
    get_box_at_ij,
    get_column,
    get_column_at_ij,
    get_row,
    get_row_at_ij,
    has_duplicates,
    shape,
)

NUMBERS: set = {1, 2, 3, 4, 5, 6, 7, 8, 9}


def find_options(s: Sudoku, exhaustive: bool = False) -> list[Option]:
    result: list[Option] = []
    m, n = shape(s)

    for i in range(m):
        for j in range(n):
            if s[i][j] is not None:
                continue

            r, c, b = (
                get_row_at_ij(s, i, j),
                get_column_at_ij(s, 0, j),
                get_box_at_ij(s, i, j),
            )

            if j < 6:
                # left
                non_options_ij = r[:9] + c + b
            if 6 <= j < 9:
                # middle
                non_options_ij = r[:6] + r[9:] + c + b
            if 9 <= j < 15:
                # right
                non_options_ij = r[6:] + c + b

            options_ij = [el for el in NUMBERS if el not in non_options_ij]
            result = add_option(result, (i, j), options_ij)

            if not exhaustive and len(options_ij) <= 1:
                return result
    return result


def is_valid(s: Sudoku) -> bool:
    m, n = shape(s)
    if m != 9 or n != 15:
        return False

    if not all([el in NUMBERS or el is None for el in flatten(s)]):
        return False

    for i in range(m):
        row, column = get_row(s, i), get_column(s, i)

        if has_duplicates(row[:9]) or has_duplicates(row[6:]) or has_duplicates(column):
            return False

    for i in range(3):
        for j in range(5):
            box = get_box_at_ij(s, i * 3, j * 3)
            if has_duplicates(box):
                return False

    return True


def build_solver() -> Solver:
    return partial(solver.solve, find_options=find_options, is_valid=is_valid)


def solve(x: Sudoku) -> Sudoku | None:
    f = build_solver()
    return f(x)
