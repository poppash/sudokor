from functools import partial, reduce
from itertools import combinations_with_replacement
from typing import Callable, Iterable, TypeAlias

import sudokor.solver as solver
from sudokor.constructors import from_shape
from sudokor.solver import add_option
from sudokor.types import Coordinate, Matrix, Option, Solver, Sudoku
from sudokor.utils import (
    enumerate_matrix,
    get_column,
    get_column_at_ij,
    get_row,
    get_row_at_ij,
    has_duplicates,
    shape,
    without_none,
)

Fn: TypeAlias = Callable[[int, int], int | float]
Constraint: TypeAlias = tuple[list[Coordinate], str, int]


def fn_sum(a: int, b: int) -> int:
    return a + b


def fn_difference(a: int, b: int) -> int:
    return a - b


def fn_product(a: int, b: int) -> int:
    return a * b


def fn_quotient(a: int, b: int) -> int | float:
    return a / b


def fn_reduce(fn: Fn, x: Iterable[int]) -> int | float:
    xs = sorted(x, reverse=fn in (fn_difference, fn_quotient))
    return reduce(fn, xs)  # type: ignore[arg-type]


OPERATIONS = {
    "+": fn_sum,  # 43
    "-": fn_difference,  # 45
    "*": fn_product,  # 42
    "/": fn_quotient,  # 47
}


def parse_constraint(constraint: Constraint) -> tuple[list[Coordinate], Fn, int]:
    coordinates, operation, y = constraint
    return (coordinates, OPERATIONS[operation], y)


def get_combinations(
    x: set[int],
    n: int,
    fn: Fn,
    y: int,
    reverse: bool = False,
) -> list[list[int]]:
    rv: list[list[int]] = []

    for combination in combinations_with_replacement(x, n):
        if fn_reduce(fn, combination) == y:
            rv = [*rv, list(combination)]
    return rv


def get_constraint_satisfying_combinations(
    s: Sudoku, constraints: list[Constraint]
) -> Matrix[list[int]]:
    m, n = shape(s)

    result = from_shape(m, n, [-1])
    x = set(range(1, m + 1))

    for constraint in constraints:
        coordinates, fn, y = parse_constraint(constraint)
        reverse = fn in [fn_difference, fn_quotient]

        combinations = get_combinations(
            x=x, n=len(coordinates), fn=fn, y=y, reverse=reverse
        )
        for i, j in coordinates:
            result[i][j] = combinations

    return result


def validate_constraints(
    s: Sudoku, constraints: list[Constraint]
) -> tuple[Sudoku, list[Constraint]]:
    # TODO: write exception messages
    m, n = shape(s)

    cover = from_shape(m, n)

    for constraint in constraints:
        coordinates, _, _ = constraint

        for i, j in coordinates:
            if cover[i][j] is not None:
                raise Exception(f"Duplicate coordinate: {(i, j)}.")
            cover[i][j] = 1

    for (i, j), cover_ij in enumerate_matrix(cover):
        if cover_ij is None:
            raise Exception(f"Missing coordinate: {(i, j)}.")

    combinations = get_constraint_satisfying_combinations(s, constraints)
    for (i, j), combinations_ij in enumerate_matrix(combinations):
        if len(combinations_ij) == 0:  # type: ignore[arg-type]
            raise Exception(f"Coordinate part of intractible constraint: {(i, j)}.")

    return s, constraints


def is_sublist(a: list[int], b: list[int]) -> bool:
    return all([a.count(el) <= b.count(el) for el in set(a)])


def subtract_list(a: list[int], b: list[int]) -> list[int]:
    result = []

    for el in set(a + b):
        n = b.count(el) - a.count(el)
        if n < 0:
            raise ValueError(f"Are you sure {a} is a sublist of {b}?")
        result += n * [el]

    return result


def get_related_coordinates(
    s: Sudoku, constraints: list[Constraint]
) -> list[list[list]]:
    m, n = shape(s)
    result = from_shape(m, n)

    for coordinates, _, _ in constraints:
        for i, j in coordinates:
            result[i][j] = coordinates
    return result


def get_related_values(s: Sudoku, coordinates: list[Coordinate]) -> list[int | None]:
    return [s[i][j] for (i, j) in coordinates]


def find_options_row_column_ij(s: Sudoku, i: int, j: int) -> set[int]:
    m, _ = shape(s)

    non_options_ij = get_row_at_ij(s, i, j) + get_column_at_ij(s, i, j)

    options_ij = set(range(1, m + 1)) - set(non_options_ij)
    return options_ij


def find_options_constraints_ij(
    s: Sudoku,
    combinations: list[list[int]],
    related_coordinates: list[Coordinate],
    i: int,
    j: int,
) -> set[int]:
    result: list[int] = []

    values = without_none(get_related_values(s, related_coordinates))

    for combination in combinations:
        if is_sublist(values, combination):
            remainder = subtract_list(values, combination)
            result = result + remainder

    return set(result)


def find_options(
    s: Sudoku,
    combinations: list,
    related_coordinates: list,
    exhaustive: bool = False,
) -> list[Option]:
    rv: list[Option] = []

    m, n = shape(s)
    for i in range(m):
        for j in range(n):
            if s[i][j] is not None:
                continue

            options_row_column_ij = find_options_row_column_ij(s, i, j)
            options_constraints_ij = find_options_constraints_ij(
                s, combinations[i][j], related_coordinates[i][j], i, j
            )

            options_ij = list(options_row_column_ij & options_constraints_ij)
            rv = add_option(rv, (i, j), options_ij)

            if not exhaustive and len(options_ij) <= 1:
                return rv
    return rv


def is_valid(s: Sudoku, constraints: list[Constraint]) -> bool:
    m, n = shape(s)

    if m != n:
        return False

    for k in range(m):
        row, column = get_row(s, k), get_column(s, k)
        if has_duplicates(row) or has_duplicates(column):
            return False

    for constraint in constraints:
        coordinates, fn, y = parse_constraint(constraint)

        values = get_related_values(s, coordinates)
        values_without_none = without_none(values)
        if len(values) != len(values_without_none):
            break

        if fn_reduce(fn, values_without_none) != y:
            return False

    return True


def build_solver(x: Sudoku, constraints: list[Constraint]) -> Solver:
    validate_constraints(x, constraints)

    combinations = get_constraint_satisfying_combinations(x, constraints)
    coordinates = get_related_coordinates(x, constraints)

    func_find_options = partial(
        find_options,
        combinations=combinations,
        related_coordinates=coordinates,
    )
    func_is_valid = partial(is_valid, constraints=constraints)

    return partial(solver.solve, find_options=func_find_options, is_valid=func_is_valid)


def solve(x: Sudoku, constraints) -> Sudoku | None:
    f = build_solver(x, constraints)
    return f(x)
