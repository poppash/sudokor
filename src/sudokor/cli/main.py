import typer

from sudokor.cli import calcudoku, sudoku, twin_sudoku

app = typer.Typer(add_completion=False)

app.add_typer(calcudoku.app, name="calcudoku")
app.add_typer(sudoku.app, name="sudoku")
app.add_typer(twin_sudoku.app, name="twin-sudoku")


if __name__ == "__main__":
    app()
