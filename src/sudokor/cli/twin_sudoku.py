from pathlib import Path

import typer

from sudokor.cli.utils import format_solution
from sudokor.constructors import from_matrix
from sudokor.types import Matrix, Sudoku
from sudokor.utils import try_or_none
from sudokor.variations.twin_sudoku import solve

app = typer.Typer()


def parse_number(x: str) -> int | None:
    return int(x) if x.strip() != "" else None


@try_or_none
def parse_row(x: str):
    values: list[str] = x.split(";")
    return [parse_number(x) for x in values]


def parse_puzzle(text: str) -> Sudoku:
    matrix: Matrix = []

    for line in text.splitlines():
        row = parse_row(line)
        if row is None:
            raise ValueError(
                "Invalid row. Each row should consist of only integers and/or empty values."  # NOQA: E501
            )

        matrix.append(row)

    return from_matrix(matrix)


@app.command(name="solve")
def solve_twin_sudoku(path: Path):
    text = path.read_text(encoding="utf-8")

    x = parse_puzzle(text)
    xs = solve(x)

    if xs is None:
        raise ValueError("It appears the provided Twin Sudoku is not solvable.")

    print(format_solution(xs))


if __name__ == "__main__":
    app()
