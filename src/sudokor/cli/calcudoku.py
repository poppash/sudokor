from pathlib import Path

import typer

from sudokor.cli.utils import format_solution
from sudokor.constructors import from_shape
from sudokor.types import Coordinate, Sudoku
from sudokor.utils import try_or_none
from sudokor.variations.calcudoku import Constraint, solve

app = typer.Typer()


@try_or_none
def parse_coordinate(x: str) -> Coordinate:
    a, b = x.strip("()").split(",")
    return (int(a), int(b))


@try_or_none
def parse_operation(x: str) -> str:
    operation = x.strip()
    assert operation in ["+", "-", "*", "/"]

    return operation


@try_or_none
def parse_target(x: str) -> int:
    return int(x)


def derive_shape_from_constraints(constraints: list[Constraint]) -> tuple[int, int]:
    max_m, max_n = -1, -1

    for coordinates, _, _ in constraints:
        max_m = max(a for a, _ in [(max_m, None), *coordinates])
        max_n = max(b for _, b in [(None, max_n), *coordinates])

    return (max_m + 1, max_n + 1)


def parse_constraints(text: str) -> list[Constraint]:
    result: list[Constraint] = []

    for line in text.splitlines():
        values: list[str] = line.split(";")

        coordinates = [parse_coordinate(coordinate) for coordinate in values[:-2]]
        operation = parse_operation(values[-2])
        target = parse_target(values[-1])

        if not all(coordinates):
            raise ValueError(
                "Invalid coordinate(s) provided. Coordinates should be tuples of two integers."  # NOQA: E501
            )

        if operation is None:
            raise ValueError(
                "Invalid operation(s) provided. Only the following operations are allowed: +, -, *, /"  # NOQA: E501
            )

        if target is None:
            raise TypeError(
                "Invalid target(s) provided. Targets must be of type integer."
            )  # NOQA: E501

        constraint: Constraint = (coordinates, operation, target)
        result.append(constraint)

    return result


def parse_puzzle(text: str) -> tuple[Sudoku, list[Constraint]]:
    constraints: list[Constraint] = parse_constraints(text)
    shape = derive_shape_from_constraints(constraints)
    return from_shape(*shape), constraints


@app.command(name="solve")
def solve_calcudoku(filepath: Path):
    text = filepath.read_text(encoding="utf-8")

    x, constraints = parse_puzzle(text)
    xs = solve(x, constraints=constraints)

    if xs is None:
        raise ValueError("It appears the provided Calcudoku is not solvable.")

    print(format_solution(xs))


if __name__ == "__main__":
    app()
