from sudokor.constructors import to_matrix
from sudokor.types import Matrix, Sudoku


def format_matrix(x: Matrix, indent: int = 4) -> str:
    return "[\n" + "\n".join([f"{indent * ' '}{row}," for row in x]) + "\n]"


def format_solution(x: Sudoku) -> str:
    return format_matrix(to_matrix(x))
