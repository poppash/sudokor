import logging
import os
from typing import Any

DEFAULT_LOGGING_LEVEL: int = logging.INFO


def NOOP(*args, **kwargs) -> None: ...


def make_basicConfig(*args, **kwargs) -> dict[str, Any]:
    config = {}

    if os.environ.get("SUDOKOR_DEBUG", "0") == "1":
        config["level"] = logging.DEBUG
    else:
        config["level"] = DEFAULT_LOGGING_LEVEL

    return config


logging.basicConfig(**make_basicConfig())
