from typing import Callable

from sudokor.constructors import from_sudoku
from sudokor.types import Coordinate, Option, Sudoku
from sudokor.utils import has_no_empty_cells


def is_solved(s: Sudoku | None, is_valid: Callable[[Sudoku], bool]) -> bool:
    if s is None:
        return False

    return has_no_empty_cells(s) and is_valid(s)


def add_option(
    options: list[Option], coordinate: Coordinate, values: list[int]
) -> list[Option]:
    return [*options, (coordinate, values)]


def pick_option(options: list[Option]) -> Option | None:
    if len(options) == 0:
        return None

    def key(option: Option) -> int:
        _, values = option
        return len(values)

    return sorted(options, key=key)[0]


def solve(
    s: Sudoku,
    find_options: Callable[[Sudoku, bool], list[Option]],
    is_valid: Callable[[Sudoku], bool],
    **kwargs,
) -> Sudoku | None:
    if not is_valid(s):
        raise ValueError("It appears the provided puzzle is invalid.")

    n, n_max = kwargs.pop("n", 0), kwargs.pop("n_max", None)
    if n == n_max:
        return s

    if is_solved(s, is_valid=is_valid):
        return s

    options_per_cell = find_options(s, exhaustive=False)  # type: ignore[call-arg]
    option = pick_option(options_per_cell)

    if option is None:
        return None

    (i, j), values = option

    n, n_max = n + 1, n_max
    for value in values:
        sc = from_sudoku(s)
        sc[i][j] = value

        result = solve(
            sc,
            find_options=find_options,
            is_valid=is_valid,
            n=n,
            n_max=n_max,
            **kwargs,
        )
        if result is not None:
            return result

    return None
