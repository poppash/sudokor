.PHONY: *

NOOP:
	:

commit-dependencies:
	git add pyproject.toml poetry.lock
	git commit -m 'Manage dependencies'

commit-formatting:
	git add .
	git commit -m 'Format'

commit-gitignore:
	git add .gitignore
	git commit -m 'Update .gitignore'

format:
	poetry run black ./src ./tests && \
	poetry run isort ./src ./tests

lab: notebook

lint:
	poetry run flake8 ./src ./tests && \
	poetry run mypy ./src ./tests

notebook:
	poetry run python -m jupyter lab

shell:
	poetry shell

suite: format lint test

test:
	poetry run python -m pytest

tests: test # alias

ui:
	poetry run sudokor-ui

# requirments
requirements.txt:
	poetry export \
		-o requirements.txt

requirements-dev.txt:
	poetry export \
		--with dev \
		-o requirements-dev.txt

requirements-test.txt:
	poetry export \
		--with test \
		-o requirements-test.txt

requirements-all.txt:
	poetry export \
		--with dev,test \
		-o requirements-all.txt

requirements: \
	requirements.txt \
	requirements-dev.txt \
	requirements-test.txt \
	requirements-all.txt
