import pytest

from sudokor.constructors import from_matrix
from sudokor.utils import (
    get_box_at_ij,
    get_column,
    get_column_at_ij,
    get_row,
    get_row_at_ij,
    has_duplicates,
    has_empty_cells,
    has_no_empty_cells,
    shape,
    without_none,
)


def test_get_row():
    data = [[1, 2, 3], [3, 1, 2], [2, 3, 1]]

    x = from_matrix(data)

    m, _ = shape(x)
    for i in range(m):
        assert data[i] == get_row(x, i)


def test_get_column():
    data = [[1, 2, 3], [3, 1, 2], [2, 3, 1]]

    x = from_matrix(data)

    _, n = shape(x)
    for j in range(n):
        assert [row[j] for row in data] == get_column(x, j)


def test_get_at_ij():
    data = [[1, 2, 3], [3, 1, 2], [2, 3, 1]]

    x = from_matrix(data)
    m, n = shape(x)

    for i in range(m):
        for j in range(n):
            assert data[i] == get_row_at_ij(x, i, j)
            assert [row[j] for row in data] == get_column_at_ij(x, i, j)

            assert data[i][j] in get_row_at_ij(x, i, j)
            assert data[i][j] in get_column_at_ij(x, i, j)


def test_get_box_at_ij():
    data = [
        [1, 1, 1, 2, 2, 2],
        [1, -1, 1, 2, -2, 2],
        [1, 1, 1, 2, 2, 2],
        [3, 3, 3, 4, 4, 4],
        [3, -3, 3, 4, -4, 4],
        [3, 3, 3, 4, 4, 4],
    ]

    x = from_matrix(data)

    for i, j in [(1, 1), (1, 4), (4, 1), (4, 4)]:
        assert data[i][j] in get_box_at_ij(x, i, j)

    assert get_box_at_ij(x, 0, 0) == get_box_at_ij(x, 2, 2)
    assert not get_box_at_ij(x, 0, 0) == get_box_at_ij(x, 2, 3)
    assert get_box_at_ij(x, 3, 3) == get_box_at_ij(x, 5, 5)
    assert not get_box_at_ij(x, 3, 3) == get_box_at_ij(x, 3, 2)

    for i0, j0 in [(0, 0), (0, 3), (3, 0), (3, 3)]:
        for i in range(3):
            for j in range(3):
                get_box_at_ij(x, i0 + i, j0 + i) == get_box_at_ij(
                    x, i0 + 2 - i, j0 + 2 - j
                )


def test_has_empty_cells():
    data = [[1, 2, 3], [3, 1, 2], [2, 3, 1]]
    data_with_none = [[1, 2, None], [2, None, 1], [None, 1, 2]]

    x = from_matrix(data)
    assert not has_empty_cells(x)
    assert has_no_empty_cells(x)

    x = from_matrix(data_with_none)
    assert not has_no_empty_cells(x)
    assert has_empty_cells(x)


def test_has_duplicates():
    assert has_duplicates([1, 1, 3])
    assert not has_duplicates([1, 2, 3])

    assert not has_duplicates([None, None, None])
    assert not has_duplicates([None, None, None], ignore_none=True)

    assert has_duplicates([None, None, None], ignore_none=False)


@pytest.mark.parametrize(
    ["x", "y"],
    [
        ([], []),
        ([1], [1]),
        ([1, 2, 3], [1, 2, 3]),
        ([None], []),
        ([1, 2, None], [1, 2]),
    ],
)
def test_without_none(x: list[int | None], y: list[int]):
    assert without_none(x) == y
