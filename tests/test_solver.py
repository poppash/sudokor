import pytest

from sudokor.constructors import from_matrix
from sudokor.solver import add_option, is_solved, pick_option, solve
from sudokor.types import Option, Sudoku
from sudokor.utils import are_equal, flatten, shape


def test_add_option():
    assert add_option([], coordinate=(0, 0), values=[42]) == [((0, 0), [42])]


@pytest.mark.parametrize(
    ["x", "y"],
    [
        ([], None),
        ([((0, 0), [0])], ((0, 0), [0])),
        ([((0, 0), [0]), ((1, 1), [1])], ((0, 0), [0])),
        ([((1, 1), [1]), ((0, 0), [0])], ((1, 1), [1])),
        ([((1, 1), [1]), ((2, 2), [2, 3]), ((3, 3), [3, 4, 5])], ((1, 1), [1])),
        ([((2, 2), [2, 3]), ((3, 3), [3, 4, 5]), ((1, 1), [1])], ((1, 1), [1])),
    ],
)
def test_pick_option(x: list[Option], y: Option):
    fx = pick_option(x)
    assert fx == y


def find_options(s: Sudoku, exhaustive: bool = False) -> list[Option]:
    rv: list[Option] = []

    m, n = shape(s)

    for i in range(m):
        for j in range(n):
            if s[i][j] is not None:
                continue

            rv = add_option(rv, (i, j), [42])

    return rv


def is_valid(s: Sudoku):
    return all([el in [42, None] for el in flatten(s)])


def test_invalid_raises_exception():
    x = from_matrix([[1, 2], [2, 1]])
    with pytest.raises(ValueError):
        solve(x, find_options=lambda _: [], is_valid=lambda _: False)


def test_is_solved():
    x1 = from_matrix([[None] * 3 for _ in range(3)])
    assert is_solved(x1, is_valid=is_valid) is False

    x2 = from_matrix([[42] * 3 for _ in range(3)])
    assert is_solved(x2, is_valid)


def test_solve():
    x = from_matrix([[None] * 3] * 3)
    y = from_matrix([[42] * 3] * 3)

    fx = solve(x, find_options=find_options, is_valid=is_valid)

    assert are_equal(fx, y)
