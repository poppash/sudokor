import logging
import os

import pytest

from sudokor import make_basicConfig


@pytest.fixture
def debug_env():
    os.environ["SUDOKOR_DEBUG"] = "1"


@pytest.fixture
def env():
    ...


def test_make_basicConfig_debug(debug_env):
    basicConfig = make_basicConfig()
    assert basicConfig["level"] == logging.DEBUG


def test_make_basicConfig(env):
    _ = make_basicConfig()
    assert True
