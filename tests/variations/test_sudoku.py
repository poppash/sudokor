import pytest

from sudokor.constructors import from_matrix
from sudokor.types import Matrix
from sudokor.utils import are_equal
from sudokor.variations.sudoku import solve

EASY: tuple[Matrix, Matrix] = (
    [
        [None, 7, 6, 5, None, None, None, None, None],
        [None, 1, None, None, None, 2, 4, 6, None],
        [None, None, 3, None, 8, 7, None, 2, 1],
        [5, None, None, 1, None, None, None, 3, 6],
        [None, None, 9, 7, None, 3, 1, None, None],
        [6, 3, None, None, None, 9, None, None, 4],
        [7, 9, None, 2, 3, None, 6, None, None],
        [None, 5, 2, 4, None, None, None, 8, None],
        [None, None, None, None, None, 5, 3, 4, None],
    ],
    [
        [2, 7, 6, 5, 4, 1, 8, 9, 3],
        [8, 1, 5, 3, 9, 2, 4, 6, 7],
        [9, 4, 3, 6, 8, 7, 5, 2, 1],
        [5, 8, 7, 1, 2, 4, 9, 3, 6],
        [4, 2, 9, 7, 6, 3, 1, 5, 8],
        [6, 3, 1, 8, 5, 9, 2, 7, 4],
        [7, 9, 4, 2, 3, 8, 6, 1, 5],
        [3, 5, 2, 4, 1, 6, 7, 8, 9],
        [1, 6, 8, 9, 7, 5, 3, 4, 2],
    ],
)

HARD: tuple[Matrix, Matrix] = (
    [
        [None, None, None, None, None, None, None, None, None],
        [None, None, None, 2, None, 5, 8, 3, 4],
        [None, None, None, None, 3, None, None, 6, 2],
        [None, 7, None, None, None, 8, 3, None, None],
        [None, None, 9, 4, None, 6, 2, None, None],
        [None, None, 6, 9, None, None, None, 7, None],
        [1, 5, None, None, 8, None, None, None, None],
        [3, 6, 7, 5, None, 4, None, None, None],
        [None, None, None, None, None, None, None, None, None],
    ],
    [
        [6, 2, 3, 8, 4, 9, 5, 1, 7],
        [7, 9, 1, 2, 6, 5, 8, 3, 4],
        [4, 8, 5, 7, 3, 1, 9, 6, 2],
        [2, 7, 4, 1, 5, 8, 3, 9, 6],
        [5, 3, 9, 4, 7, 6, 2, 8, 1],
        [8, 1, 6, 9, 2, 3, 4, 7, 5],
        [1, 5, 2, 3, 8, 7, 6, 4, 9],
        [3, 6, 7, 5, 9, 4, 1, 2, 8],
        [9, 4, 8, 6, 1, 2, 7, 5, 3],
    ],
)


@pytest.mark.parametrize("data", [EASY, HARD])
def test_solve(data: tuple[Matrix, Matrix]):
    x, y = map(from_matrix, data)
    fx = solve(x)

    assert are_equal(fx, y)
