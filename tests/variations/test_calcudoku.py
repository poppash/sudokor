import pytest

from sudokor.constructors import from_matrix, from_shape
from sudokor.types import Matrix
from sudokor.utils import are_equal
from sudokor.variations.calcudoku import (
    Constraint,
    Fn,
    fn_difference,
    fn_product,
    fn_quotient,
    fn_sum,
    get_combinations,
    get_related_coordinates,
    is_sublist,
    solve,
    subtract_list,
)

DATA: tuple[Matrix, Matrix] = (
    from_shape(6),
    [
        [2, 5, 1, 6, 3, 4],
        [6, 3, 2, 4, 1, 5],
        [5, 4, 6, 1, 2, 3],
        [3, 6, 5, 2, 4, 1],
        [4, 1, 3, 5, 6, 2],
        [1, 2, 4, 3, 5, 6],
    ],
)

CONSTRAINTS: list[Constraint] = [
    ([(0, 0), (1, 0), (2, 0), (1, 1), (2, 1)], "+", 20),
    ([(3, 0), (4, 0), (5, 0)], "+", 8),
    ([(0, 1), (0, 2)], "/", 5),
    ([(3, 1), (2, 2), (3, 2)], "+", 17),
    ([(4, 1), (4, 2)], "/", 3),
    ([(5, 1), (5, 2)], "-", 2),
    ([(0, 3), (1, 2), (1, 3), (2, 3)], "*", 48),
    ([(3, 3), (2, 4), (3, 4), (4, 4), (5, 4), (2, 5)], "+", 22),
    ([(4, 3), (5, 3)], "-", 2),
    ([(0, 4), (1, 4), (0, 5), (1, 5)], "+", 13),
    ([(3, 5), (4, 5), (5, 5)], "*", 12),
]


def test_solve():
    x, y = [from_matrix(x) for x in DATA]

    fx = solve(x, CONSTRAINTS)
    assert are_equal(fx, y)


@pytest.mark.parametrize(
    "fn,a,b,result",
    [
        (fn_sum, 0, 0, 0),
        (fn_difference, 0, 0, 0),
        (fn_product, 0, 0, 0),
        (fn_quotient, 0, 0, None),
    ],
)
def test_fn(fn, a, b, result):
    if result is None:
        with pytest.raises(Exception):
            fn(a, b)
    else:
        assert fn(a, b) == result


@pytest.mark.parametrize(
    "x,n,fn,y,result",
    [
        (
            {1, 2, 3, 4, 5, 6},
            3,
            fn_sum,
            8,
            [[1, 1, 6], [1, 2, 5], [1, 3, 4], [2, 2, 4], [2, 3, 3]],
        ),
        (
            {1, 2, 3, 4, 5, 6},
            3,
            fn_difference,
            2,
            [[4, 1, 1], [5, 2, 1], [6, 3, 1], [6, 2, 2]],
        ),
        (
            {1, 2, 3, 4, 5, 6},
            3,
            fn_product,
            12,
            [[1, 2, 6], [1, 3, 4], [2, 2, 3]],
        ),
        (
            # fmt: off
            {1, 2, 3, 4, 5, 6},
            3,
            fn_quotient,
            2,
            [[2, 1, 1], [4, 2, 1], [6, 3, 1]]
            # fmt: on
        ),
    ],
)
def test_get_combinations(x: set[int], n: int, fn: Fn, y: int, result: list[list[int]]):
    reverse = fn in [fn_difference, fn_quotient]
    assert [sorted(c) for c in get_combinations(x, n, fn, y, reverse=reverse)] == [
        sorted(c) for c in result
    ]


@pytest.mark.parametrize(
    "a,b,result",
    [
        ([], [], True),
        ([], [1, 2, 3], True),
        ([1, 2, 3], [], False),
        ([1, 2, 3], [1, 2, 3], True),
        ([1, 2, 2, 3], [1, 2, 3, 4], False),
    ],
)
def test_is_sublist(a: list[int], b: list[int], result: bool):
    assert is_sublist(a, b) is result


@pytest.mark.parametrize(
    "a,b,result",
    [
        ([], [], []),
        ([], [1, 2, 3], [1, 2, 3]),
        ([1, 2, 3], [1, 2, 3], []),
        ([1, 2, 3], [1, 1, 2, 2, 3, 3], [1, 2, 3]),
    ],
)
def test_subtract_list(a: list[int], b: list[int], result: list[int]):
    assert subtract_list(a, b) == result


def test_subtract_list_raises_exception():
    with pytest.raises(ValueError):
        subtract_list([1, 2, 3], [])


def test_get_related_coordinates():
    m, n = 3, 3
    data = [[None] * n] * m
    s = from_matrix(data)

    constraints = [
        ([(0, 0), (1, 0), (0, 1)], fn_sum, 9),
        ([(2, 0)], fn_sum, 1),
        ([(1, 1), (0, 2), (1, 2)], fn_sum, 4),
        ([(2, 1), (2, 2)], fn_sum, 5),
    ]

    x1 = get_related_coordinates(s, constraints)
    x2 = [
        # fmt: off
        [
            [(0, 0), (1, 0), (0, 1)],
            [(0, 0), (1, 0), (0, 1)],
            [(1, 1), (0, 2), (1, 2)],
        ],
        [
            [(0, 0), (1, 0), (0, 1)],
            [(1, 1), (0, 2), (1, 2)],
            [(1, 1), (0, 2), (1, 2)],
        ],
        [
            [(2, 0)],
            [(2, 1), (2, 2)],
            [(2, 1), (2, 2)]
        ],
        # fmt: on
    ]
    assert x1 == x2
