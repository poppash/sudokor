import pytest

from sudokor.constructors import from_matrix
from sudokor.types import Matrix
from sudokor.utils import are_equal
from sudokor.variations.twin_sudoku import build_solver

DATA: tuple[Matrix, Matrix] = (
    [
        [None, 5, 3, 4, None, 2, 1, None, None, 4, None, 6, 5, 9, 3],
        [None, 1, 7, None, None, 6, None, 2, 4, 9, None, 5, 7, 6, 1],
        [None, 2, 8, None, None, 7, None, 5, None, 1, None, None, None, 4, 8],
        [None, 4, 2, None, None, 1, None, None, None, 6, 5, 4, None, 2, None],
        [1, 6, 5, None, None, None, None, 4, 7, 3, 1, None, 6, None, 5],
        [None, 8, 9, 2, None, 4, None, 6, None, 8, None, 2, 4, None, 9],
        [2, None, None, 1, None, None, 7, 8, None, 2, 4, 3, None, None, 6],
        [5, 3, 6, None, None, 8, 4, 1, 9, None, 6, None, None, None, None],
        [None, None, 1, 9, None, 5, None, None, 2, 7, 9, 1, None, None, None],
    ],
    [
        [6, 5, 3, 4, 9, 2, 1, 7, 8, 4, 2, 6, 5, 9, 3],
        [9, 1, 7, 5, 8, 6, 3, 2, 4, 9, 8, 5, 7, 6, 1],
        [4, 2, 8, 3, 1, 7, 9, 5, 6, 1, 3, 7, 2, 4, 8],
        [7, 4, 2, 6, 5, 1, 8, 9, 3, 6, 5, 4, 1, 2, 7],
        [1, 6, 5, 8, 3, 9, 2, 4, 7, 3, 1, 9, 6, 8, 5],
        [3, 8, 9, 2, 7, 4, 5, 6, 1, 8, 7, 2, 4, 3, 9],
        [2, 9, 4, 1, 6, 3, 7, 8, 5, 2, 4, 3, 9, 1, 6],
        [5, 3, 6, 7, 2, 8, 4, 1, 9, 5, 6, 8, 3, 7, 2],
        [8, 7, 1, 9, 4, 5, 6, 3, 2, 7, 9, 1, 8, 5, 4],
    ],
)


@pytest.mark.parametrize("data", [DATA])
def test_solve_twin_sudoku(data: tuple[Matrix, Matrix]):
    x, y = map(from_matrix, data)
    solve = build_solver()

    fx = solve(x)

    assert are_equal(fx, y)
