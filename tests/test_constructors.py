from typing import get_origin

from sudokor.constructors import from_csv, from_matrix, from_shape, from_sudoku
from sudokor.types import Sudoku
from sudokor.utils import are_equal, flatten


def test_from_matrix():
    data = [[1, 2, None], [2, None, 1], [None, 1, 2]]

    s = from_matrix(data)

    assert isinstance(s, get_origin(Sudoku))
    for i in range(3):
        for j in range(3):
            assert data[i][j] == s[i][j]


def test_from_csv():
    csv = """
    1,2,
    2,,1
    ,1,2
    """

    x = from_csv(csv)
    assert isinstance(x, get_origin(Sudoku))
    assert x[0][0] == 1
    assert x[0][2] is None


def test_from_sudoku():
    data = [[1, 2, None], [2, None, 1], [None, 1, 2]]

    s = from_matrix(data)
    s_new = from_sudoku(s)

    assert are_equal(s, s_new)
    assert isinstance(s, get_origin(Sudoku))


def test_from_sudoku_empty():
    data = [[42] * 3] * 3

    s = from_sudoku(from_matrix(data), empty=True)
    assert isinstance(s, get_origin(Sudoku))
    assert all([el is None for el in flatten(s)])


def test_from_shape():
    m, n = 3, 3
    assert isinstance(from_shape(m, n), get_origin(Sudoku))

    m, n = 9, None
    assert isinstance(from_shape(m), get_origin(Sudoku))
