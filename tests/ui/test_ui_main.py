from sudokor.ui.buttons import QuitButton
from sudokor.ui.main import APP_GEOMETRY


def test_window_title(ui):
    assert ui.windowTitle() == "Sudokor UI"


def test_geometry_was_changed(ui):
    x, y, _, _ = APP_GEOMETRY
    geometry = ui.geometry()

    assert geometry.x() != x and geometry.y() != y


def test_has_quit_button(ui):
    assert any([isinstance(c, QuitButton) for c in ui.controls.children()])


def test_can_instatiate_properly(ui, sudoku, twin_sudoku, calcudoku):
    assert True  # reaching this statement means all is well, so far
