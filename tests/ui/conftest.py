import sys

import pytest
from PyQt5.QtWidgets import QApplication, QWidget

from sudokor.ui.main import SudokorUi
from sudokor.ui.puzzles import CalcudokuPuzzle, SudokuPuzzle, TwinSudokuPuzzle


@pytest.fixture()
def app():
    return QApplication(sys.argv)


@pytest.fixture
def ui(app):
    ui = SudokorUi()

    assert ui is not None
    assert isinstance(ui, QWidget)

    return ui


@pytest.fixture
def sudoku(app):
    puzzle = SudokuPuzzle()

    assert puzzle is not None
    assert isinstance(puzzle, QWidget)

    return puzzle


@pytest.fixture
def twin_sudoku(app):
    puzzle = TwinSudokuPuzzle()

    assert puzzle is not None
    assert isinstance(puzzle, QWidget)

    return puzzle


@pytest.fixture
def calcudoku(app):
    puzzle = CalcudokuPuzzle()

    assert puzzle is not None
    assert isinstance(puzzle, QWidget)

    return puzzle
