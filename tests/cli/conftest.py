import os

import pytest
from typer.testing import CliRunner


@pytest.fixture
def data_dir():
    return f"{os.path.dirname(__file__)}/data"


@pytest.fixture
def runner():
    return CliRunner()
