import pytest
from typer.testing import CliRunner

from sudokor.cli.main import app
from tests.cli.utils import assert_exception_was_raised, assert_stdout_contains_solution

runner = CliRunner()


def test_app_help():
    result = runner.invoke(app, ["--help"])
    assert result.exit_code == 0


def test_app_twin_sudoku(data_dir):
    filepath_to_puzzle = f"{data_dir}/twin_sudoku/puzzle.csv"
    result = runner.invoke(app, ["twin-sudoku", "solve", filepath_to_puzzle])

    assert result.exit_code == 0
    solution = [
        [6, 5, 3, 4, 9, 2, 1, 7, 8, 4, 2, 6, 5, 9, 3],
        [9, 1, 7, 5, 8, 6, 3, 2, 4, 9, 8, 5, 7, 6, 1],
        [4, 2, 8, 3, 1, 7, 9, 5, 6, 1, 3, 7, 2, 4, 8],
        [7, 4, 2, 6, 5, 1, 8, 9, 3, 6, 5, 4, 1, 2, 7],
        [1, 6, 5, 8, 3, 9, 2, 4, 7, 3, 1, 9, 6, 8, 5],
        [3, 8, 9, 2, 7, 4, 5, 6, 1, 8, 7, 2, 4, 3, 9],
        [2, 9, 4, 1, 6, 3, 7, 8, 5, 2, 4, 3, 9, 1, 6],
        [5, 3, 6, 7, 2, 8, 4, 1, 9, 5, 6, 8, 3, 7, 2],
        [8, 7, 1, 9, 4, 5, 6, 3, 2, 7, 9, 1, 8, 5, 4],
    ]
    assert_stdout_contains_solution(result, solution)


@pytest.mark.skip()
@pytest.mark.parametrize("", [])
def test_app_twin_sudoku_with_invalid_input():
    ...


def test_app_calcudoku(data_dir):
    filepath_to_puzzle = f"{data_dir}/calcudoku/constraints.csv"
    result = runner.invoke(app, ["calcudoku", "solve", filepath_to_puzzle])
    assert result.exit_code == 0


@pytest.mark.parametrize(
    "basename,exception_type,exception_message",
    [
        (
            "constraints_with_invalid_operations.csv",
            ValueError,
            "Invalid operation(s) provided.",
        ),
        (
            "constraints_with_invalid_coordinates.csv",
            ValueError,
            "Invalid coordinate(s) provided.",
        ),
        (
            "constraints_with_invalid_targets.csv",
            TypeError,
            "Invalid target(s) provided.",
        ),
        (
            "constraints_with_missing_coordinates.csv",
            Exception,
            "Missing coordinate:",
        ),
        (
            "constraints_with_duplicate_coordinates.csv",
            Exception,
            "Duplicate coordinate:",
        ),
        (
            "constraints_intractible.csv",
            Exception,
            "Coordinate part of intractible constraint:",
        ),
    ],
)
def test_app_calcudoku_invalid_input(
    data_dir, basename, exception_type, exception_message
):
    filepath_to_puzzle = f"{data_dir}/calcudoku/{basename}"
    result = runner.invoke(app, ["calcudoku", "solve", filepath_to_puzzle])
    assert_exception_was_raised(result, exception_type, exception_message)
