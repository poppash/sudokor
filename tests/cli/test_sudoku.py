import pytest

from sudokor.cli import app
from tests.cli.utils import assert_stdout_contains_solution


def test_app_sudoku(data_dir, runner):
    filepath_to_puzzle = f"{data_dir}/sudoku/puzzle.csv"
    result = runner.invoke(app, ["sudoku", "solve", filepath_to_puzzle])
    assert result.exit_code == 0
    solution = [
        [2, 7, 6, 5, 4, 1, 8, 9, 3],
        [8, 1, 5, 3, 9, 2, 4, 6, 7],
        [9, 4, 3, 6, 8, 7, 5, 2, 1],
        [5, 8, 7, 1, 2, 4, 9, 3, 6],
        [4, 2, 9, 7, 6, 3, 1, 5, 8],
        [6, 3, 1, 8, 5, 9, 2, 7, 4],
        [7, 9, 4, 2, 3, 8, 6, 1, 5],
        [3, 5, 2, 4, 1, 6, 7, 8, 9],
        [1, 6, 8, 9, 7, 5, 3, 4, 2],
    ]
    assert_stdout_contains_solution(result, solution)


@pytest.mark.skip()
@pytest.mark.parametrize("", [])
def test_app_sudoku_with_invalid_input():
    ...
