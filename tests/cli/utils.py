from typer.testing import Result

from sudokor.types import Matrix


def assert_stdout_contains_solution(result: Result, solution: Matrix) -> None:
    assert all([str(row) in result.stdout for row in solution])


def assert_exception_was_raised(
    result: Result,
    exception_type=Exception,
    exception_message: str | None = None,
    strict: bool = False,
) -> None:
    assert result.exception
    assert type(result.exception) is exception_type

    if exception_message is not None:
        message = str(result.exception)
        assert exception_message == message if strict else exception_message in message
