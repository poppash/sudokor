# Sudokor

Solve puzzles!

## Installation

```sh
# clone the repo, enter directory
git clone git@gitlab.com:poppash/sudokor.git
cd sudokor

# create virtual environment
python -m pip install virtualenv
python -m virtualenv env
source env/bin/activate

# install poetry, install package OR
python -m pip install poetry
poetry install

# install through pip
python -m pip install -r requirements.txt
# python -m pip install -r requirements-dev.txt -r requirements-test.txt # in case you also want to install dev and test dependencies
python -m pip install .
```
